﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Notebook;

namespace add
{
    public class Person
    {
        public static Dictionary<int, Person> note = new Dictionary<int, Person>();
        private static int count = 0;
        public Person() { this.Id = Person.count; Person.count += 1; }
        public Person(int i) { this.Zero = 1; }

        public int Id { get; }
        public string Secondname { set; get; }
        public string Name { set; get; }
        public string Thirdname { set; get; }
        public string Number { set; get; }
        public string Country { set; get; }
        public DateTime Birthday { set; get; }
        public string Organisation { set; get; }
        public string Post { set; get; }
        public string Other { set; get; }
        public byte Zero  { set; get;}
        

       internal static void add()
        {
            Console.Clear();

            Person person = new Person();
            Console.WriteLine("***************Переходим с созданию записи**************");
            fill(person);

            note.Add(person.Id, person);
            Console.WriteLine("Для продолжения нажмите любую кнопку..."); Console.ReadKey(); Console.Clear();
          
            
        }
        internal static void change()
        {
               Person person=check();
            if (person.Zero != 1)
            {
                Console.WriteLine("************ Переходим к изменению записи********");
                fill(person);
                 Console.ReadKey(); Console.Clear();
            }
               

        }
        internal static void remove()
        {
            Console.WriteLine("Введите,пожалуйста, ID записи для удаления");int id;
            do
            {
                try
                {
                    id = Convert.ToInt32(Console.ReadLine()); break;
                }
                catch (Exception x)
                {
                    Console.WriteLine("Повторите ввод еще раз.Введенное не является числом!");
                }
            } while (true);
            bool o = false;
          
            for (int i=0;i< note.Count;i++)
            {
                if (id == note.ElementAt(i).Key) {
                    note.Remove(i); o = true; Console.WriteLine("Запись по Id найдена и удалена!"); Console.WriteLine("Для продолжения нажмите любую кнопку..."); Console.ReadKey(); 
                Console.Clear(); }
            }
            if (o == false) { Console.WriteLine("Запись по Id не найдена.Переход в главное меню"); Console.WriteLine("Для продолжения нажмите любую кнопку..."); Console.ReadKey(); }

        }

        internal static void showone()

        {
            Person x = check();
                   if ((x.Zero!= 1))
            {
               
                show(x);Console.WriteLine("Для продолжения работы нажмите любую кнопку...");
                Console.ReadKey();Console.Clear();

            }
                   
            }


        internal static void showall()
        {
            Console.Clear();
            do {
                if (note.Count() == 0) { Console.WriteLine("Наша  электронная записная книга пуста !"); break; }
                Console.Clear();
                foreach (KeyValuePair<int, Person> box in note)
                {
                    Console.WriteLine("Id -" + box.Key + "\n Имя - " + box.Value.Name + "\n Фамилия - " + box.Value.Secondname + "\n Номер телефона - " + box.Value.Number); 
                }
                break;
            }while(true);
            Console.WriteLine("Для продолжения нажмите любую кнопку...");
            Console.ReadKey(); Console.Clear();
        } 
           

        
      private  static void show(Person person)
        {
            Console.WriteLine("Имя - " + person.Name + "\n" + "Фамилия - " + person.Secondname + "\n" + "Отчество - " + person.Thirdname + "\n" + "Номер телефона - " + person.Number + "\n" + "Страна - " + person.Country + "\n" + "Дата рождения - " + person.Birthday.ToShortDateString() + "\n" + "Организация " + person.Organisation + "\n" + "Должность - " + person.Post + "\nПрочие заметки - " + person.Other);
        }

        static Person check()
        {Person person1 = new Person(32);bool o = false;
           
                Console.WriteLine("Введите,пожалуйста, ID записи");int id;
            do
            {
                try
                {
                    id = Convert.ToInt32(Console.ReadLine()); break;
                }
                catch (Exception x) { Console.WriteLine("Неккоретный Id, повторите ввод!"); }
            } while (true);
                
                foreach (KeyValuePair<int, Person> box in note)
                {
                    if (id == box.Key) { person1 = box.Value; o = true; Console.WriteLine("Запись по Id найдена.Нажмите любую кнопку для продолжения...");break; }
                }
                if (o == false) { Console.Clear(); Console.WriteLine("Запись по Id не найдена.Переход в главное меню."); Console.WriteLine("Для продолжения нажмите любую кнопку..."); Console.ReadKey(); }
            Console.Clear();
            return person1;
        }
      private  static Person fill(Person person )
        { 
            string number = "1234567890+";
         
          
            Console.WriteLine("Id -"+person.Id);
            Console.WriteLine("Введите фамилию");

            person.Secondname = Console.ReadLine(); 
           
            while (person.Secondname.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели.Повторите");
                person.Secondname = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine("Введите имя");
            person.Name = Console.ReadLine();
            while (person.Name.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.Name = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine("Введите отчество");
            person.Thirdname = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Введите номер телефона");
            person.Number = Console.ReadLine();int l ;
            
            do
            {
                 l= 1;
                
                for (int i = 0; i < person.Number.Length; i++)
                {
                    if ((number.Contains(person.Number[i]))==false) { l = 0; 
                    }
                }
                if ((l == 1)&&(person.Number.Length>5)) { break; }
                Console.WriteLine("Введите номер еще раз, так как текущее значение меньше пяти знаков или содержит буквы или пробелы");
                person.Number = Console.ReadLine();

            } while ((person.Number.Length < 5) || (l == 0));
          
            Console.Clear();
            Console.WriteLine("Введите страну");
            person.Country = Console.ReadLine();
            while (person.Country.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.Country = Console.ReadLine();
            }
            Console.Clear();
            do
            {
                try
                {
                    Console.WriteLine("Введите дату рождения");
                    person.Birthday = DateTime.Parse(Console.ReadLine()); break;
                }
                catch (Exception x) { Console.WriteLine("Повторите ввод даты рождения"); Console.WriteLine("Для продолжения нажмите любую кнопку..."); Console.ReadKey();Console.Clear(); }
            } while (true);
            Console.Clear();
            Console.WriteLine("Введите организацию");
            person.Organisation = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите должность");
            person.Post = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введиие прочие заметки");
            person.Other = Console.ReadLine();
            return person;
        }
    }
}

